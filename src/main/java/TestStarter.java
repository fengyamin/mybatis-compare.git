import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.mybatisflex.core.query.QueryWrapper;
import flex.FlexInitializer;
import flex.entity.FlexAccount;
import plus.PlusInitializer;
import plus.entity.PlusAccount;

import static flex.entity.table.Tables.FLEX_ACCOUNT;

public class TestStarter {

    private static final int queryCount = 1000;

    public static void main(String[] args) {

        System.out.println("---------------------初始化开始---------------------");
        FlexInitializer.init();
        PlusInitializer.init();
        System.out.println("---------------------初始化结束---------------------");


        System.out.println("---------------------预热开始---------------------");


        //预热防止起跑线不一致情况
        testFlexSelectOne();
        testPlusSelectOneWithLambda();
        testPlusSelectOne();
        testFlexPaginate();
        testPlusPaginate();
        testFlexUpdate();
        testPlusUpdate();
        System.out.println("---------------------预热结束---------------------");

        long timeMillis;


        System.out.println();
        System.out.println();
        System.out.println("---------------------开始测试 查询单个selectOne:---------------------");

        for (int i = 0; i < 5; i++) {
            System.out.println("---------------");

            timeMillis = System.currentTimeMillis();
            testFlexSelectOne();
            System.out.println(">>>>>>>testFlex-SelectOne:" + (System.currentTimeMillis() - timeMillis));

            timeMillis = System.currentTimeMillis();
            testPlusSelectOneWithLambda();
            System.out.println(">>>>>>>testPlus-SelectOneWithLambda:" + (System.currentTimeMillis() - timeMillis));

            timeMillis = System.currentTimeMillis();
            testPlusSelectOne();
            System.out.println(">>>>>>>testPlus-SelectOne:" + (System.currentTimeMillis() - timeMillis));
        }


        System.out.println();
        System.out.println();
        System.out.println("---------------------开始测试 查询多个selectList:---------------------");

        for (int i = 0; i < 5; i++) {
            System.out.println("---------------");

            timeMillis = System.currentTimeMillis();
            testFlexSelectTop10();
            System.out.println(">>>>>>>testFlex-SelectTop10:" + (System.currentTimeMillis() - timeMillis));

            timeMillis = System.currentTimeMillis();
            testPlusSelectTop10WithLambda();
            System.out.println(">>>>>>>testPlus-SelectTop10WithLambda:" + (System.currentTimeMillis() - timeMillis));

            timeMillis = System.currentTimeMillis();
            testPlusSelectTop10();
            System.out.println(">>>>>>>testPlus-SelectTop10:" + (System.currentTimeMillis() - timeMillis));
        }


        System.out.println();
        System.out.println();
        System.out.println("---------------------开始测试 查询分页paginate:---------------------");

        for (int i = 0; i < 5; i++) {
            System.out.println("---------------");

            timeMillis = System.currentTimeMillis();
            testFlexPaginate();
            System.out.println(">>>>>>>testFlex-Paginate:" + (System.currentTimeMillis() - timeMillis));

            timeMillis = System.currentTimeMillis();
            testPlusPaginate();
            System.out.println(">>>>>>>testPlus-Paginate:" + (System.currentTimeMillis() - timeMillis));
        }


        System.out.println();
        System.out.println();
        System.out.println("---------------------开始测试 更新update:---------------------");

        for (int i = 0; i < 5; i++) {
            System.out.println("---------------");

            timeMillis = System.currentTimeMillis();
            testFlexUpdate();
            System.out.println(">>>>>>>testFlex-Update:" + (System.currentTimeMillis() - timeMillis));

            timeMillis = System.currentTimeMillis();
            testPlusUpdate();
            System.out.println(">>>>>>>testPlus-Update:" + (System.currentTimeMillis() - timeMillis));
        }
    }


    private static void testFlexSelectOne() {
        for (int i = 0; i < queryCount; i++) {
            FlexInitializer.selectOne();
        }
    }

    private static void testPlusSelectOneWithLambda() {
        for (int i = 0; i < queryCount; i++) {
            PlusInitializer.selectOneWithLambda();
        }
    }

    private static void testPlusSelectOne() {
        for (int i = 0; i < queryCount; i++) {
            PlusInitializer.selectOne();
        }
    }


    private static void testFlexSelectTop10() {
        for (int i = 0; i < queryCount; i++) {
            FlexInitializer.selectTop10();
        }
    }

    private static void testPlusSelectTop10WithLambda() {
        for (int i = 0; i < queryCount; i++) {
            PlusInitializer.selectTop10WithLambda();
        }
    }

    private static void testPlusSelectTop10() {
        for (int i = 0; i < queryCount; i++) {
            PlusInitializer.selectTop10();
        }
    }


    private static void testFlexPaginate() {
        for (int i = 1; i <= queryCount; i++) {
            QueryWrapper queryWrapper = new QueryWrapper()
                    .where(FLEX_ACCOUNT.ID.ge(100));
            FlexInitializer.paginate(1, 10, queryWrapper);
        }
    }

    private static void testPlusPaginate() {
        for (int i = 1; i <= queryCount; i++) {
            LambdaQueryWrapper<PlusAccount> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.ge(PlusAccount::getId, 100);
            PlusInitializer.paginate(1, 10, queryWrapper);
        }
    }

    private static void testFlexUpdate() {
        for (long i = 0; i < queryCount; i++) {
            FlexAccount flexAccount = new FlexAccount();
            flexAccount.setUserName("testInsert" + i);
            flexAccount.setNickname("testInsert" + i);
            flexAccount.addOption("key1", "value1");
            flexAccount.addOption("key2", "value2");
            flexAccount.addOption("key3", "value3");
            flexAccount.addOption("key4", "value4");
            flexAccount.addOption("key5", "value5");

            QueryWrapper queryWrapper = QueryWrapper.create()
                    .where(FLEX_ACCOUNT.ID.ge(9200))
                    .and(FLEX_ACCOUNT.ID.le(9300))
                    .and(FLEX_ACCOUNT.USER_NAME.like("admin"))
                    .and(FLEX_ACCOUNT.NICKNAME.like("admin"));

            FlexInitializer.update(flexAccount, queryWrapper);
        }
    }


    private static void testPlusUpdate() {
        for (int i = 0; i < queryCount; i++) {
            PlusAccount plusAccount = new PlusAccount();
            plusAccount.setUserName("testInsert" + i);
            plusAccount.setNickname("testInsert" + i);
            plusAccount.addOption("key1", "value1");
            plusAccount.addOption("key2", "value2");
            plusAccount.addOption("key3", "value3");
            plusAccount.addOption("key4", "value4");
            plusAccount.addOption("key5", "value5");

            LambdaUpdateWrapper<PlusAccount> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.ge(PlusAccount::getId, 9000);
            updateWrapper.le(PlusAccount::getId, 9100);
            updateWrapper.like(PlusAccount::getUserName, "admin");
            updateWrapper.like(PlusAccount::getNickname, "admin");
            PlusInitializer.update(plusAccount, updateWrapper);
        }
    }


}
